package com.marathon.interview;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class InterviewApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder().sources(InterviewApplication.class)
                .run(args);
    }

}
