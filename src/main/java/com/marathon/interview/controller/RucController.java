package com.marathon.interview.controller;

import com.marathon.interview.domain.model.Company;
import com.marathon.interview.domain.service.SRUCService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RucController {
    private final Logger logger = LoggerFactory.getLogger(RucController.class);
    private final SRUCService srucService;

    @Autowired
    public RucController(SRUCService srucService) {
        this.srucService = srucService;
    }

    @GetMapping("/")
    public String root() {
        logger.debug("root return /user/ruc");
        return "/user/ruc";
    }

    @GetMapping("/login")
    public String login() {
        logger.debug("/login return login");
        return "login";
    }

    @GetMapping("/access-denied")
    public String accessDenied() {
        logger.debug("/access-denied return error/access-denied");
        return "error/access-denied";
    }

    @GetMapping(value = "/consultaRuc")
    public ModelAndView consultaRuc(@RequestParam(value = "ruc", required = true) String ruc,
                                    Model model, ModelAndView modelAndView) {
        logger.debug("/consultaRuc return ModelAndView");
        Company empresa = srucService.queryCompanyByRuc(ruc);
        model.addAttribute("empresa", empresa);
        model.addAttribute("random", "perrito");
        modelAndView.getModelMap().addAttribute("random2", "gatito");
        modelAndView.setViewName("/user/empresa");

        return modelAndView;
    }
}
