package com.marathon.interview.repository.api;

import com.marathon.interview.config.InterviewConfig;
import com.marathon.interview.domain.model.Company;
import com.marathon.interview.domain.repository.CompanyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@EnableConfigurationProperties(InterviewConfig.class)
public class CompanyRepositoryRestClient implements CompanyRepository {
    private final Logger logger = LoggerFactory.getLogger(CompanyRepositoryRestClient.class);
    private final String TYPE_PARAM = "tipo";
    private final String RUC = "ruc";
    private final String TOKEN = "token";

    private final RestTemplate restTemplate;
    private final InterviewConfig config;

    @Autowired
    public CompanyRepositoryRestClient(RestTemplate restTemplate, InterviewConfig config) {
        this.restTemplate = restTemplate;
        this.config = config;
    }


    @Override
    public Company getCompanyByRuc(String ruc) {
        logger.debug("REST GET {} with params {}", config.getEndpoint());

        CompanyExt result = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl(config.getEndpoint())
                        .queryParam(TYPE_PARAM, 2)
                        .queryParam(RUC, ruc)
                        .queryParam(TOKEN, config.getToken()
                        ).toUriString(),
                CompanyExt.class);
        logger.debug("Result : {}", result);

        return result.buildDomain();
    }
}
