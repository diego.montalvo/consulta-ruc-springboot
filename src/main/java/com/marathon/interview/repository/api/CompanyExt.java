package com.marathon.interview.repository.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.marathon.interview.domain.model.Company;
import com.marathon.interview.domain.model.Status;

import java.util.StringJoiner;

public class CompanyExt {

    private String ruc;
    @JsonProperty(value = "razon_social")
    private String razonSocial;
    private Status estado;
    private String direccion;
    private String ubigeo;

    private String departamento;
    private String provincia;
    private String distrito;

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Status getEstado() {
        return estado;
    }

    public void setEstado(Status estado) {
        this.estado = estado;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public Company buildDomain() {
        return new Company(ruc, razonSocial, estado, direccion, ubigeo, departamento, provincia, distrito);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CompanyExt.class.getSimpleName() + "[", "]")
                .add("ruc='" + ruc + "'")
                .add("razonSocial='" + razonSocial + "'")
                .add("estado=" + estado)
                .add("direccion='" + direccion + "'")
                .add("ubigeo='" + ubigeo + "'")
                .add("departamento='" + departamento + "'")
                .add("provincia='" + provincia + "'")
                .add("distrito='" + distrito + "'")
                .toString();
    }
}
