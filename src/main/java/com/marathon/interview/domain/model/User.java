package com.marathon.interview.domain.model;

import java.time.LocalDateTime;

public class User {

    private Integer id;
    private String username;
    private String name;
    private String password;
    private LocalDateTime creationTime;
}
