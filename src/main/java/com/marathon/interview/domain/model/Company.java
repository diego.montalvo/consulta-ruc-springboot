package com.marathon.interview.domain.model;

public class Company {

    private String ruc;
    private String razonSocial;
    private Status status;
    private String address;
    private String addressCode;
    private String department;
    private String province;
    private String district;

    public Company() {
    }

    public Company(String ruc, String razonSocial, Status status, String address, String addressCode, String department, String province, String district) {
        this.ruc = ruc;
        this.razonSocial = razonSocial;
        this.status = status;
        this.address = address;
        this.addressCode = addressCode;
        this.department = department;
        this.province = province;
        this.district = district;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getAddressCode() {
        return addressCode;
    }

    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
