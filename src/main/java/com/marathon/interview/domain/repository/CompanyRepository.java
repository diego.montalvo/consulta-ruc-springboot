package com.marathon.interview.domain.repository;

import com.marathon.interview.domain.model.Company;

public interface CompanyRepository {

    public Company getCompanyByRuc(String ruc);

}
