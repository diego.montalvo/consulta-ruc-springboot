package com.marathon.interview.domain.service;

import com.marathon.interview.domain.model.Company;
import com.marathon.interview.domain.repository.CompanyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SRUCServiceImpl implements SRUCService {

    private final Logger logger = LoggerFactory.getLogger(SRUCServiceImpl.class);
    private final CompanyRepository companyRepository;


    @Autowired
    public SRUCServiceImpl(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Override
    public Company queryCompanyByRuc(String ruc) {
        logger.debug("Query Company By RUC : {}", ruc);
        return companyRepository.getCompanyByRuc(ruc);
    }
}
