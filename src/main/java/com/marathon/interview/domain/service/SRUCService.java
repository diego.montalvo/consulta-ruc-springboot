package com.marathon.interview.domain.service;

import com.marathon.interview.domain.model.Company;

public interface SRUCService {

    public Company queryCompanyByRuc(String ruc);
}
