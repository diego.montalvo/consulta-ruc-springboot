package com.marathon.interview.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@ConfigurationProperties(prefix = "interview.sunat")
public class InterviewConfig {

    private String endpoint;
    private String token;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getToken() {
        return token;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
